(in-package :vecto-graphs)

(defmethod call-with-graph ((backend (eql :vecto)) file width height function)
  (vecto:with-canvas (:width width :height height)
    (setf (font *font-size*) *font*)
    (funcall function)
    (vecto:save-png file)))

;;;

(defmethod b-set-stroke-color ((backend (eql :vecto)) r g b)
  (vecto:set-rgb-stroke r g b))

(defmethod b-set-fill-color ((backend (eql :vecto)) r g b)
  (vecto:set-rgb-fill r g b))

(defmethod b-draw-line* ((backend (eql :vecto)) from-x from-y to-x to-y)
  (vecto:move-to from-x from-y)
  (vecto:line-to to-x to-y)
  (vecto:stroke))

(defmethod b-set-font ((backend (eql :vecto)) font size)
  (vecto:set-font font size))

(defmethod b-%draw-string ((backend (eql :vecto)) x y string)
  (vecto:draw-string x y string))

(defmethod b-string-box ((backend (eql :vecto)) string)
  (vecto:string-bounding-box (ensure-string string) (font-size) (font)))

(defmethod b-ymax ((backend (eql :vecto)) bbox)
  (zpb-ttf:ymax bbox))

(defmethod b-xmax ((backend (eql :vecto)) bbox)
  (zpb-ttf:xmax bbox))

(defmethod b-ymin ((backend (eql :vecto)) bbox)
  (zpb-ttf:ymin bbox))

(defmethod b-xmin ((backend (eql :vecto)) bbox)
  (zpb-ttf:xmin bbox))

(defmethod b-draw-rectangle ((backend (eql :vecto)) origin width height)
  (vecto:rectangle (x origin) (y origin) width height)
  (vecto:fill-and-stroke))

(defmethod b-translate ((backend (eql :vecto)) point)
  (vecto:translate (x point) (y point)))

(defmethod b-rotate ((backend (eql :vecto)) angle)
  (vecto:rotate angle))

(defmethod b-arc ((backend (eql :vecto)) center radius start-angle end-angle)
  (vecto:arc (x center) (y center)
             radius start-angle end-angle)
  (vecto:fill-and-stroke))

(defmethod b-move-to ((backend (eql :vecto)) point)
  (vecto:move-to (x point) (y point)))

(defmethod b-line-to ((backend (eql :vecto)) point)
  (vecto:line-to (x point) (y point)))

(defmethod b-stroke ((backend (eql :vecto)))
  (vecto:stroke))
