(in-package :vecto-graphs)

(defvar *axes-color* (parse-color "606060"))
(defvar *legend-box-width* 8)

(defun draw-axis-label (origin
                        axis-length
                        labels
                        orientation
                        step)
  (apply #'set-stroke-color *axes-color*)
  (let ((axis-start (ecase orientation
                      (:x (x origin))
                      (:y (y origin)))))
    (loop with step = (if step
                          (* axis-length step)
                          (floor axis-length (1+ (length labels))))
          for x from (+ axis-start step) by step
          for label in labels
          do
          (ecase orientation
            (:x
             (draw-line* x (- (y origin) 4)
                         x (y origin))
             (draw-string (point x (- (y origin) 6))
                          label
                          :align-x :center
                          :align-y :top
                          :rotate :anti-clockwise))
            (:y
             (draw-line* (- (x origin) 4) x
                         (x origin) x)
             (draw-string (point (- (x origin) 6) x)
                          label
                          :align-x :right
                          :align-y :center))))))

(defun legend-width (labels)
  (let ((number (length labels)))
    (+ (reduce #'+ labels :key #'string-width)
       (* number (+ *legend-box-width* 2)) ;; 2 for the space after
       (* (1- number) *legend-box-width*)))) ;; spaces between

(defun split-legend (labels)
  (let ((max-width (- *width* (* *margins* 2))))
    (loop for width = 0
          while labels
          collect
          (loop for spacer = 0 then *legend-box-width*
                for label = (car labels)
                while (and labels
                           (< (incf width (+ (string-width label)
                                             *legend-box-width*
                                             spacer))
                              max-width))
                collect label
                do
                (pop labels)))))

(defun draw-bar-legend (labels)
  (let ((colors *colors*))
    (flet ((draw-line (labels y)
             (let* ((width (legend-width labels))
                    (start (max (- (/ *width* 2) (/ width 2))
                                *margins*)))
               (loop for label in labels
                     for color = (pop colors)
                     do
                     (apply #'set-fill-color color)
                     (apply #'set-stroke-color color)
                     (draw-rectangle (point start y) *legend-box-width* *axis-font-size*)
                     (set-fill-color 0 0 0)
                     (set-stroke-color 0 0 0)
                     (incf start (+ *legend-box-width* 2))
                     (incf start (+ (bbox-width (draw-string (point start y)
                                                             label))
                                    *legend-box-width*))))))
      (let* ((split (split-legend labels))
             (leading (* *axis-font-size* *leading-ratio*))
             (height (* (length split) leading)))
        (loop for line in split
              for y downfrom (+ *margins* height (- leading)) by leading
              do (draw-line line y))
        height))))

(defun draw-labels (x-label y-label
                    origin x-axis-length y-axis-length)
  (when (stringp x-label)
    (draw-string (point (+ (x origin) (/ x-axis-length 2))
                        *margins*)
                 x-label
                 :align-x :center))
  (when (stringp y-label)
    (draw-string (point *margins*
                        (+ (y origin) (/ y-axis-length 2)))
                 y-label
                 :rotate :anti-clockwise
                 :align-y :center)))

(defun calculate-margins (xs ys x-label y-label legend-height)
  (let* ((x-height (if (stringp x-label)
                       (string-height x-label)
                       legend-height))
         (y-height (if y-label
                       (string-height y-label)
                       0))
         (max-x-label-length (ceiling (max-label-length xs :x)))
         (max-y-label-length (ceiling (max-label-length ys :x)))
         (x (thin-line
             (+ (* *margins* 3)
                y-height
                3
                max-y-label-length
                3)))
         (y (thin-line
             (+ (* *margins* 3)
                x-height
                3
                max-x-label-length
                3))))
    (values (point x y)
            (- *width* x *margins*)
            (- *height* y *margins*))))

(defvar *arrow-length* 6)

(defun draw-arrows (origin x-axis-length y-axis-length)
  (let ((x-axis-y (+ y-axis-length (y origin)))
        (y-axis-x (+ x-axis-length (x origin)))
        (opposite (* (tan (/ pi 5)) *arrow-length*)))
    (draw-line* (x origin)
                x-axis-y
                (+ (x origin) opposite)
                (- x-axis-y *arrow-length*))
    (draw-line* (x origin)
                x-axis-y
                (- (x origin) opposite)
                (- x-axis-y *arrow-length*))
    (draw-line* y-axis-x
                (y origin)
                (- y-axis-x *arrow-length*)
                (+ (y origin) opposite))
    (draw-line* y-axis-x
                (y origin)
                (- y-axis-x *arrow-length*)
                (- (y origin) opposite))))

(defun draw-axes-lines (xs ys x-label y-label legend-height &key arrows)
  (apply #'set-stroke-color *axes-color*)
  (multiple-value-bind (origin
                        x-axis-length
                        y-axis-length)
      (calculate-margins xs ys x-label y-label legend-height)
    (draw-line origin
               (add-x origin x-axis-length))
    (draw-line origin
               (add-y origin y-axis-length))
    (when arrows
      (draw-arrows origin x-axis-length y-axis-length))
    (values origin x-axis-length y-axis-length)))

(defun draw-axes  (xs ys x-label y-label
                   &key y-step
                        arrows)
  (setf (font-size) *axis-font-size*)
  (let ((legend-height (if (consp x-label)
                           (draw-bar-legend x-label)
                           0)))
    (multiple-value-bind (origin x-axis-length y-axis-length)
        (draw-axes-lines xs ys x-label y-label legend-height
                         :arrows arrows)
      (draw-labels x-label y-label
                   origin
                   x-axis-length y-axis-length)
      (setf (font-size) *axis-font-size*)
      (draw-axis-label origin
                       x-axis-length
                       xs
                       :x
                       nil)
      (draw-axis-label origin y-axis-length
                       ys
                       :y
                       y-step)
      (values origin x-axis-length y-axis-length))))

(defun max-label-length (labels orientation &key key)
  (loop for label in labels
        for box = (string-box (if key
                                  (funcall key label)
                                  label))
        maximize (ecase orientation
                   (:x
                    (- (xmax box)
                       (xmin box)))
                   (:y
                    (- (ymax box)
                       (ymin box))))))

(defun total-label-length (labels orientation &key key)
  (loop for label in labels
        for box = (string-box (if key
                                  (funcall key label)
                                  label))
        sum (ecase orientation
              (:x
               (- (xmax box)
                  (xmin box)))
              (:y
               (- (ymax box)
                  (ymin box))))))

(defun labels-from-numbers (step max &key percentage)
  (loop for i from step by step below max
        collect (format-number i percentage)))

(defvar *max-label-length* 200)

(defun abbreviate-labels (labels)
  (flet ((abbreviate (label)
           (with-output-to-string (out)
             (loop for char across label
                   for width = (string-width (string char))
                   for total-width = width then (+ total-width width)
                   if (> total-width *max-label-length*)
                   do (princ "..." out)
                      (return)
                   do
                   (write-char char out)))))
    (mapcar #'abbreviate labels)))

(defun draw-number-axes (xs max-y divisions x-label y-label
                         &key arrows percentage)
  (let ((y-step (/ max-y divisions)))
    (set-stroke-color 0 0 0)
    (draw-axes (abbreviate-labels xs)
               (and (plusp max-y)
                    (labels-from-numbers y-step max-y
                                         :percentage percentage))
               x-label
               y-label
               :y-step (and (plusp max-y)
                            (/ y-step max-y))
               :arrows arrows)))
